! function () {

	var _this = null;

	function todo () {
		_this = this;
		_this.app = document.getElementById('app');
		_this.form = _this.generate('<form class="todo-form"><input type="text" placeholder="Add todo..."><input type="submit" value="&#10133;"></form>');
		_this.form.addEventListener('submit', _this.submit);
		_this.app.appendChild(_this.form);
		window['og_loader'] = "loaded";
	}

	todo.prototype.generate = function (template, actions, fns) {
		var tmp = document.createElement('DIV');
		tmp.innerHTML = template;
		return tmp.firstElementChild;
	}

	todo.prototype.submit = function (e) {
		e.preventDefault();
		var data = this.querySelector('input[type="text"]');
		if(data.value.trim()) {
			var task = _this.generate('<div class="todo-item"></div>');
			task.innerHTML = data.value;
			task.addEventListener('click', _this.remove);
			_this.app.appendChild(task);
		}
		data.value = "";
	}

	todo.prototype.remove = function () {
		var t = this;
		t.parentNode.removeChild(t);		
	}

	window['app'] = new todo();

}();

